package org.efrasiyab

object Application extends App {

  val model = MemoryDataModel(args(0))
  val threshold = if(args.length == 2) args(1).toDouble else 0.01
  
  val engine = new MinHashRecEngine(model, 100)

  for(line <- io.Source.stdin.getLines){
    val items = line.split(",").toList.map(_.toInt)
    val res = engine.recommend(items, threshold)
    println(s"Recommending $res for input $items")
  }

}
