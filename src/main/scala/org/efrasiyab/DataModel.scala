package org.efrasiyab

trait DataModel[User, Item] {

  def getItems(user: User): Seq[Item]
  def getUsers: Seq[User]

}

class MemoryDataModel(map: Map[Int, Seq[Int]]) extends DataModel[Int, Int] {
  def getItems(user: Int): Seq[Int] = map.get(user).getOrElse(Seq.empty)
  def getUsers: Seq[Int] = map.keys.toSeq
}

object MemoryDataModel {

  def apply(path: String): MemoryDataModel = {
    var map: Map[Int, Seq[Int]] = Map.empty
    val iter = scala.io.Source.fromFile(path).getLines
    iter.foreach { line =>
      val pair = line.split(";")
      val userId: Int = pair(0).toInt
      val itemId = pair(1).toInt
      map = map + (userId -> (itemId +: map.get(userId).getOrElse(Seq.empty)))
    }
    new MemoryDataModel(map)
  }

}
