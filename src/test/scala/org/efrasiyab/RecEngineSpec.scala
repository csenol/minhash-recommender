package org.efrasiyab

import org.scalatest.WordSpec

class RecEngineSpec extends WordSpec with RecommendationBehavior {

  val mockEngine = new RecEngine[Int, Int] {
    val model = mockModel

    protected def similarCandidates(items: Seq[Int]): Seq[Int] = {
      model.getUsers
    }

    protected def getSimilarity(userId: Int, items: Seq[Int]): Option[Double] = {
      val intersection = (model.getItems(userId) intersect items).size
      if (userId == 0)
        None
      else {
        val res = Some(intersection.toDouble)
        res
      }
    }
  }

  recommendByUser(mockEngine)
  recommendByItems(mockEngine)
  recommendIrrelevantUsers(mockEngine)

}

trait RecommendationBehavior { this: WordSpec =>

  val mockModel = new DataModel[Int, Int] {
    val map: Map[Int, Seq[Int]] =
      Map(
        1 -> Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 10),
        2 -> Seq(2, 4, 6, 8, 10),
        3 -> Seq(2, 11, 12),
        4 -> Seq(20, 30, 40))
    def getUsers = map.keys.toSeq
    def getItems(id: Int) = map.get(id).getOrElse(Seq.empty)
  }

  def recommendByUser(mockEngine: RecEngine[Int, Int]) = {
    "Recommendation by User" should {
      val results = mockEngine.recommend(2, 0.1)
      "produce recommendation for similars by userId" in {
        assert(results.size > 0)
      }

    }

  }

  def recommendByItems(mockEngine: RecEngine[Int, Int]) = {
    "Recommendation by Items" should {
      val results = mockEngine.recommend(Seq(1, 2, 3, 5), 0.2)

      "produce recommendation for similars by Items" in {
        assert(results.size > 0)
      }

    }
  }

  def recommendIrrelevantUsers(mockEngine: RecEngine[Int, Int]) = {
    "Recommendation for Irrelevant Users" should {
      "not produce recommendation for unrelated users" in {
        val results = mockEngine.recommend(4, 0.2)
        assert(results.size === 0)
      }
    }

  }
}

